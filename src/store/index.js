import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  notes: [],
  activeNote: {}
}

const getters = {
  activeNoteText (state) {
    return state.activeNote.text
  },

  favoriteNotes (state) {
    return state.notes.filter(note => note.favorite)
  },

  noteCount (state) {
    return state.notes.length
  }
}

export const mutations = {
  addNote (state) {
    const newNote = {
      text: 'New note',
      favorite: false
    }

    state.notes.push(newNote)
    state.activeNote = newNote
  },

  editNote (state, evt) {
    state.activeNote.text = evt.target.value
  },

  deleteNote (state) {
    state.notes.splice(state.notes.indexOf(state.activeNote), 1)
    state.activeNote = state.notes[0] || {}
  },

  toggleFavorite (state) {
    state.activeNote.favorite = !state.activeNote.favorite
  },

  setActiveNote (state, note) {
    state.activeNote = note
  }
}

export const options = {
  state,
  getters,
  mutations
}

export default new Vuex.Store(options)
