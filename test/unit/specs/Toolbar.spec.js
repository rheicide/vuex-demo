import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash'
import Toolbar from '@/components/Toolbar'
import { options } from '@/store'

describe('Toolbar.vue', () => {
  let vm, store

  beforeEach(() => {
    store = new Vuex.Store(_.cloneDeep(options))
    const Constructor = Vue.extend({ ...Toolbar, store })
    vm = new Constructor().$mount()
  })

  it('should render correct contents', () => {
    expect(vm.$el.querySelectorAll('i').length).to.equal(3)
  })

  it('should be able to add a new note', () => {
    vm.addNote()
    expect(store.state.notes.length).to.equal(1)
    expect(vm.activeNote.text).to.equal('New note')
    expect(vm.activeNote.favorite).to.equal(false)
  })

  it('should be able to delete the active note', () => {
    vm.addNote()
    expect(store.state.notes.length).to.equal(1)

    vm.deleteNote()
    expect(store.state.notes.length).to.equal(0)
  })

  it('should be able to toggle the "favorite" state of the active note', () => {
    vm.addNote()
    expect(store.state.activeNote.favorite).to.equal(false)

    vm.toggleFavorite()
    expect(store.state.activeNote.favorite).to.equal(true)
  })

  it('should show the star icon in a different style when the active note is favorite', (done) => {
    vm.addNote()
    vm.toggleFavorite()
    vm.$nextTick()
      .then(() => {
        expect(vm.$el.querySelector('.glyphicon-star').className).to.include('starred')
        done()
      })
      .catch(done)
  })
})
