import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash'
import NoteList from '@/components/NoteList'
import { options } from '@/store'

describe('NoteList.vue', () => {
  let vm, store

  beforeEach(() => {
    store = new Vuex.Store(_.cloneDeep(options))
    const Constructor = Vue.extend({ ...NoteList, store })
    vm = new Constructor().$mount()

    store.commit('addNote')
    store.commit('addNote')
    store.commit('addNote')
  })

  it('should render correct contents', () => {
    expect(vm.$el.querySelector('h2').textContent).to.contain('Notes')
  })

  it('should show correct notes when All or Favorites is selected', () => {
    store.commit('toggleFavorite')

    vm.show = 'all'
    expect(vm.filteredNotes.length).to.equal(3)

    vm.show = 'favorites'
    expect(vm.filteredNotes.length).to.equal(1)

    vm.show = 'wtf'
    expect(vm.filteredNotes.length).to.equal(0)
  })

  it('should set active note correctly', (done) => {
    expect(vm.activeNote).to.equal(vm.notes[2])

    vm.setActiveNote(vm.notes[0])
    vm.$nextTick()
      .then(() => {
        expect(vm.$el.querySelector('.list-group-item').className).to.include('active')
        done()
      })
      .catch(done)
  })
})
