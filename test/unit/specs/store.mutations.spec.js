import { mutations } from '@/store'

describe('mutations', () => {
  it('should edit a note correctly', () => {
    const { editNote } = mutations
    const state = { activeNote: { text: 'before' } }
    const event = { target: { value: 'after' } }

    editNote(state, event)
    expect(state.activeNote.text).to.equal('after')
  })
})
